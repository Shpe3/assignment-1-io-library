%define STDOUT 1
%define STDIN 0

%define SYSCALL_PRINT 1
%define SYSCALL_READ 0
%define SYSCALL_EXIT 60

%define NULL_TERM 0
%define NEW_STRING 10
%define SPACE 32
%define TAB 9
%define MINUS 45
%define PLUS 43
%define MIN 48
%define MAX 57

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], NULL_TERM
        je .end
        inc rax
        jmp .loop
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rax, SYSCALL_PRINT
    mov rsi, rdi
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_STRING
    call print_string
    ret

; Принимает код символа и выводит его в stdout 
print_char:
    push rdi
    mov rax, SYSCALL_PRINT
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    js .negat
    .pos:
        jmp print_uint
    .negat:
        push rdi
        mov rdi, MINUS
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    mov rax, rdi
    mov r8, 10
    mov rcx, rsp
    sub rsp, 32
    dec rcx
    mov byte [rcx], NULL_TERM
    .loop:
        xor rdx, rdx
        div r8
        add rdx, MIN
        dec rcx
        mov byte[rcx], dl
        test rax, rax
        jne .loop
    .print:
        mov rdi, rcx
        call print_string
        add rsp, 32
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
        mov r9b, byte[rdi+rcx]
        cmp r9b, byte[rsi+rcx]
        jne .not_equal
    .term_check:
        cmp r9b, NULL_TERM
        je .equal
        inc rcx
        jmp .loop
    .equal:
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYSCALL_READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov r8, rax
    pop rax
    cmp r8, -1
    je .error
    jmp .success
    .error:
        xor rax, rax
        test r8, r8
        jne .end
    .success:
        mov rdx, 1
    .end:
        xor r8, r8
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r13
    push r14
    push r15
    mov r13, rdi
    mov r14, rsi
    xor r15, r15

    .skip_whitespace:
        call read_char
        cmp rax, TAB
        je .skip_whitespace
        cmp rax, NEW_STRING
        je .skip_whitespace
        cmp rax, SPACE
        je .skip_whitespace
    .read:
        cmp r15, r14
        je .overflow
        test al, al
        je .success
        cmp al, TAB
        je .success
        cmp al, NEW_STRING
        je .success
        cmp al, SPACE
        je .success
        mov byte [r13 + r15], al
        inc r15
        call read_char
        jmp .read
    .success:
        mov byte [r13 + r15], NULL_TERM
        mov rax, r13
        mov rdx, r15
        pop r15
        pop r14
        pop r13
        ret
    .overflow:
        xor rax, rax
        pop r15
        pop r14
        pop r13
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r8, r8
    xor r9, r9

    .loop:
        mov r9b, byte[rdi + r8]
        cmp r9b, NULL_TERM
        je .end
        cmp r9b, MIN
        jl .end
        cmp r9b, MAX
        jg .end
        inc r8
        sub r9b, MIN
        mul r10
        add rax, r9
        jmp .loop
    .end:
        mov rdx, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r9, r9
    mov r9b, byte[rdi]
    cmp r9b, MINUS
    je .neg
    push rdi
    call parse_uint
    pop rdi
    test r9b, PLUS
    jne .end
    inc rdx
    jmp .end
    .neg:
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    neg rax
    inc rdx
    .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jg .overflow
    xor rax, rax
    xor r11, r11
    .loop:
        mov r11b, byte [rdi + rax]
        mov byte [rsi + rax], r11b
        cmp r11b, NULL_TERM
        je .success
        inc rax
        jmp .loop
    .overflow:
        xor rax, rax
    .success:
        ret